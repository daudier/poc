$(document).ready(function() {
	//stackBlurImage("blur", "canvas", 7, false);

	// might need need to match for Edge - IE11 technical preview
	// Detecting IE browser: http://www.petri.com/using-internet-explorer-11-edge-mode.htm
	// Detecting Android native browser: http://stackoverflow.com/questions/9286355/how-to-detect-only-the-native-android-browser

	var nua = navigator.userAgent;

	var isIE = (nua.toLowerCase().indexOf('msie') != -1) || (!!nua.match(/Trident|Edge/));
	var isAndroid = ((nua.indexOf('Mozilla/5.0') > -1 && nua.indexOf('Android ') > -1 && nua.indexOf('AppleWebKit') > -1) && !(nua.indexOf('Chrome') > -1));

	if (isIE || isAndroid) {
		stackBlurImage("blur", "canvas", 7, false);
	}


	// Mapbox

	L.mapbox.accessToken = 'pk.eyJ1IjoiamRhdWRpZXIiLCJhIjoiNVBCS1R0WSJ9.U8DZcjZ36sqCa-5kbSbUqw';

	// Here we don't use the second argument to map, since that would automatically
	// load in non-clustered markers from the layer. Instead we add just the
	// backing tileLayer, and then use the featureLayer only for its data.
	var map = L.mapbox.map('map')
		.setView([37.614, -119.553], 6)
		.addLayer(L.mapbox.tileLayer('jdaudier.l6n3919c'));

	// As with any other AJAX request, this technique is subject to the Same Origin Policy:
	// http://en.wikipedia.org/wiki/Same_origin_policy
	// So the JSON file must be on the same domain as the Javascript, or the server
	// delivering it should support CORS.
	var outlineLayer = L.mapbox.featureLayer()
		.loadURL('js/california.geojson')
		.addTo(map);

	// Since featureLayer is an asynchronous method, we use the `.on('ready'`
	// call to only use its marker data once we know it is actually loaded.
	// markerLayer.on('ready', function(e) {

	var markerLayer = L.mapbox.featureLayer()
		.loadURL('js/markers.geojson')
		.on('ready', function(e) {

			//	The clusterGroup gets each marker in the group added to it
			//	once loaded, and then is added to the map
			var markers = new L.MarkerClusterGroup({
					showCoverageOnHover: false
				}
			);
			e.target.eachLayer(function(layer) {
				markers.addLayer(layer);
			});

			markers.on('clustermouseover', function (a) {
				var allLayers = a.target.getLayers();
				var lastLayer = allLayers.pop();
				var region = lastLayer.feature.properties.region;

				var lat = markers.getVisibleParent(a.layer).getLatLng().lat;
				var lng = markers.getVisibleParent(a.layer).getLatLng().lng;
				var latlng = L.latLng(lat, lng);

				var popup = L.popup({
					closeButton: false
				})
					.setLatLng(latlng)
					.setContent(region)
					.openOn(map);

				this.on('clusterclick', function() {
					map.closePopup();
				})
			});

			map.addLayer(markers)
				.fitBounds(markers.getBounds());
		});

	map.scrollWheelZoom.disable();
});



